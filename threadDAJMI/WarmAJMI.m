function [selected_AJMI2,time_AJMI2] = ColdAJMI(dataindex, k, b, M, pre_selected, pre_time) 
%This is the DAJMI without caching, but with a cold start
%input: dataindex is the data, k is the amount of features selected, b is the bootstrapping indices, M is amount of worker available
%input: pre_selected is the selected_feature set as cold start, pre-time is the time
%output: selected features and time vector



%Data preparation%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fid = fopen('/home/hengl/matlab/bin/scripts/DAJMI/names2');
file_name = {};
for i=1:30
    line = fgets(fid);
    if ischar(line)
        file_name{i} = line;
    end
end
expression = '[A-Za-z0-9-_]*';
for i=1:30
    str = regexp(file_name{i}, expression);
    file_name{i} = file_name{i}(1: str(1,2)-2);
end
path = strcat('/home/hengl/matlab/bin/scripts/DAJMI/data/', file_name{dataindex}, '.mat');
file = load(path);
data = struct2cell(file);
data = data{1};
f = size(data,2)-1;
data = data(b,:);
y = data(:, size(data,2));

%Initialization%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
time_AJMI2 = pre_time;
selected_AJMI2 = pre_selected; %keep the selected feature subset

feature_set = [1:f]; 
feature_set = setdiff(feature_set, selected_AJMI2);

parpool(4); %start the parallel pool and distributing the feature space
poolobj = gcp;
addAttachedFiles(poolobj,{'mi.m', 'joint.m'});

start = tic; %Start the clock
%Prepare for parallelzation%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sizeBlock = ceil(size(feature_set,2)/M);
score_dist_features = zeros(M, sizeBlock); %this is used to store the scores for each candidate
dist_feature_set = [];   %this is used to store the features, the rows is the indices for worker
parfor i = 1:M-1
    index = ((i-1)*sizeBlock+1):i*sizeBlock;
    dist_feature_set(i,:) = feature_set(index);
end
index = ((M-1)*sizeBlock+1):size(feature_set,2);
dist_feature_set(M,:) = [feature_set(index) zeros(1,(sizeBlock - size(index,2)))]; %denote the empty spots as 0



%selecting k features%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
while size(selected_AJMI2,2) <= k %select k or more features  
    %calculating the score of each candidate feature
    parfor i = 1:M  
        features = dist_feature_set(i,:);
        scores = score_dist_features(i,:); %old scores
        temp_scores = [];
        for p = features
            temp = 0;
            if p ~= 0 %we denote the selected/unavailable features as 0
                for q = selected_AJMI2
                   temp = temp + mi(joint([data(:,p),data(:,q)],[]),y);
                end
                temp_scores = [temp_scores temp];
            else
                temp_scores = [temp_scores -1];
            end
        end
        score_dist_features(i,:) = temp_scores;
    end
    
    %sort the candidate features and get the sequence
    scores = score_dist_features'; 
    scores = scores(:);
    [B, I] = sort(scores, 'descend');
    sorted_features = feature_set(I(B >= 0));
    B = B(B >= 0);

    %for each candidate calculate its boundary and verify the candidates in parallel 
    flag = ones(1, M+1); %1 means no intrusion
    intruders = zeros(1, M+1); %mark the intruders
    
    parfor i = 2:M+1 
        boundary = 0;
        pre = []; %joint informativeness with features located ahead
        for s = 1:i-1 %find the boundary of the interfering zone 
            temp = mi(joint([data(:,sorted_features(i)),data(:,sorted_features(s))],[]), y);
            pre = [pre temp];
            boundary = boundary + temp - h(y); 
        end
        
        windowsize = 1;
        for p = i+1:size(sorted_features,2)
            if B(i) - B(p) + boundary < 0
                windowsize = windowsize + 1;
            else
                break;
            end
        end   
        windowsize = windowsize + i;
        if windowsize  > size(sorted_features,2)
            windowsize = size(sorted_features,2)
        end
        
        intrude_max_objective = 0; % to keep track of the score of the strongest intruder
        for p = i+1:windowsize %iterate all possible intruders in the IZ
            objective = B(i) - B(p) + sum(pre);
            for q = 1:i-1 %each candidate is evaluated w.r.t. its predecessor 
                objective =  objective - mi(joint([data(:,sorted_features(p)),data(:,sorted_features(q))],[]), y);
            end
            if objective < 0 
                flag(i) = 0;
                if (B(i) + sum(pre) - objective) > intrude_max_objective;
                    intruders(i) = sorted_features(p);
                    intrude_max_objective = B(i) + sum(pre) - objective;
                end
            end 
        end 
    end
    
    %selecting the good candidates
    previous_selected = [];
    for i = 1:size(flag,2)
        if flag(i) == 1
            previous_selected = [previous_selected sorted_features(i)];
        else
            break;
        end       
    end
    index = find(flag == 0);
    if size(index,2) > 0
        previous_selected = [previous_selected intruders(index(1))];
    end

    parfor i = 1:M
        features = dist_feature_set(i,:);
        scores = score_dist_features(i,:);
        index = 0;
        for j = previous_selected
            index = find(features == j);
            features(index) = 0;
            scores(index) = -1;
        end
        dist_feature_set(i,:) = features;
        score_dist_features(i,:) = scores;
    end
    selected_AJMI2 = [selected_AJMI2 previous_selected];
    
    disp('selecting...')
    disp(previous_selected)
    disp(size(selected_AJMI2,2))
    
    
    time_AJMI2(size(selected_AJMI2,2)) = toc(start) + pre_time(30);
end

delete(gcp('nocreate'));

