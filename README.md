%This is a project verifing the Distributed Aggregating Joint mutual information(AJMI) feature selection methodology
% AJMI.m/JMI.m is the original version
% AJMIcached.m/JMIcached.m is using the caching scheme to avoid the redudant calculation
% ColdAJMI.m is the AJMI using Cold start
% ColdAJMIcached.m is the AJMI using Cold-start and caching schemes
There experiment strategy is as follows:
%%%%%%%%%%%%%%%%%%%%%%%%%%%  ColdAJMI.m <------> JMI.m  %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%  ColdAJMIcached.m <------> JMIcached.m %%%%%%%%%%%%%%%%%%%%%%%%%%%
